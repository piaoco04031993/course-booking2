import { useContext, useEffect } from 'react';

import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

export default function Logout() {

	const { setUser, unsetUser } = useContext (UserContext);

	unsetUser();

	useEffect(() => {
		// update the global state of the user.
		setUser({
			id: null,
			isAdmin: null
		})
	}, [setUser]);

	return(
		<Navigate to="/login" replace={true}/>
	)
}