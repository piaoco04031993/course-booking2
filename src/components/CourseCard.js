//identify needed Components
import { Card, Col } from 'react-bootstrap';

//element with routing capability

import { Link } from 'react-router-dom';
// we are going to use this link component to not only redirect the user the course page but to carry data needed to identify

export default function CourseCard({courseProp}) {
	console.log(courseProp)
	return(
			
				<Col xs={12} md={4}>
					<Card>
						<Card.Body>
							<Card.Title>
								{courseProp.name}
							</Card.Title>
							<Card.Text>
								{courseProp.description}
							</Card.Text>
							<Card.Text>
								Price: {courseProp.price}
							</Card.Text>
							<Link to={`view/${courseProp._id}`} className="btn btn-primary">
								View Course </Link>
						</Card.Body>
					</Card>
				</Col>

	);
};