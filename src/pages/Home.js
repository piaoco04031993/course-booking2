//acquire all the compnents that will make up teh home page. (hero section, highlights)
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

//Lets create a data object the content of the hero section

const data = {
	title: 'Welcome to the Home Page',
	content: 'Opportunities for Everyone, Everywhere'
}	

export default function Home() {
	return(
	 <>
		<Banner bannerData={data}/>
		<Highlights />
	 </>
	);
};